// I/O 

use std::io;
fn main() {
    println!("Hello, world!");

    let mut result = String::new();

    io::stdin().read_line(&mut result).expect("failed to read input");
    println!("{}", result);


}
