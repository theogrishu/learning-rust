// arithmetic and type casting

use std::io;
fn main() {
    let x: i8 = 24;
    let y: i32 = 34;

    // for any arithmetic operation type of all operands must be same
    // x:i8 + y:i32 willl throw error

    // type conversion
    println!("sum of x and y is: {}", x as i32 + y);

    let a = 24i32;
    println!("sum of a and y is: {}", a + y);

    let b = 12_000_000_i64;
    println!("sum of a and b is: {}", a as i64 + b);

    // --- String to int or float conversion ---
    let mut number = String::new();
    io::stdin()
        .read_line(&mut number)
        .expect("Error reading input");

    let int_input: i64 = number.trim().parse().unwrap();

    println!("square of your input is: {}",  int_input * int_input);
}
