// variables, cconstants and shadowing

fn main() {
    // ----VARIABLES----
    // declare using let
    // by default var are immutable
    // can be implicitly typed
    let number = 4;
    // make then mutable by using 'mut'
    println!("number is: {}", number); // 4

    let mut double = number * 2;
    println!("double is: {}", double); // 4
    double = 0;
    println!("now after mutation double is: {}", double); // 0

    // or just redeclare the variables to mutate without using mut
    let number = 80;

    println!("after redeclaring number is: {}", number); // 80

    // while redclaring they can be assigned different type
    let number = "string";
    println!("number is now: {}", number);

    // ----CONSTANTS----
    // must be capital snake casse
    // are immutable
    // cannot be redclared
    // are explicitly typed
    const SECONDS_IN_MINUTES: u32 = 60;
    println!("there are '{}' seconds in one minute", SECONDS_IN_MINUTES);

    // ----SHADOWING----

    let x = 5;
    println!("{}", x); // 5

    {
        let x = 0;
        // this x is only available in current scope
        println!("{}", x); // 0
    }

    println!("{}", x); //  still 5
    let x = 55;
    println!("{}", x); // 55

    {
        // internal scope can access outer sccope
        let x = x * 10;
        println!("{}", x); // 550
    }

    println!("{}", x); // 55
}
