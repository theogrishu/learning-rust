// DATA TYPES
fn main() {
    // PRIMITIVE TYPES => Scalar & Compound

    // Scalar Types => int, float, bool, characters
    // - represents single value

    // Compound types => arrays and tuple
    // - can group multiple values in one type

    let unsigned_number: u8 = 128;
    let signed_number: i64 = -40;
    let float_number: f64 = 3.14;

    // use single quotes for char and double for string
    let my_char: char = 's';

    println!("{}", unsigned_number);
    println!("{}", signed_number);
    println!("{}", float_number);
    println!("{}", my_char);

    // ---- Arrays -----

    // an array can have only one type of values
    // legal => [1,2,3,4]
    // illegal => [1,"rust-lang", 'c', true] , [1,false]
    // arrays in Rust have a fixed length.

    let mut my_array = [1, 2, 3, 4];
    let my_array_with_explicit_types: [f64; 6] = [1.1, 1.2, 1.3, 1.4, 1.5, 1.6];

    println!(
        "3rd item in my_array is: {} & the length of my_array is {}",
        my_array[2],
        my_array.len()
    );
    println!(
        "length of my_array_with_explicit_types is {}",
        my_array_with_explicit_types.len()
    );

    my_array[1] = my_array[1] * 10;

    println!("2nd item in my_array changed to {}", my_array[1]);

    // -----TUPLE-----

    // unlike arrays tuples can have elements with different types
    // fixed length like arrays
    let mut my_tuple = (1, "string");
    let my_tuple_with_explicit_type: (i32, bool, &str) = (1, true, "string");
    let my_tuple_with_array_inside: (&str, [i32; 4]) = ("rust", [1, 2, 3, 4]);

    // unlike array acces values inside tuple using dot `.`
    println!("{}", my_tuple.0);
    println!("{}", my_tuple_with_explicit_type.2);
    println!("{}", my_tuple_with_array_inside.1[3]);

    println!("----- mutating tuple -----");

    my_tuple.0 = 100;
    println!("after mutating first element in tuple is: {}", my_tuple.0);

    let tuple_in_array: [(u32, u32); 3] = [(2, 4), (3, 9), (4, 16)];
    println!(
        "Square of {} is: {}",
        tuple_in_array[0].0, tuple_in_array[0].1
    )
}
